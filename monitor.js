const os = require('os');

// Get cpu(s) info
function getCpus() {
  return os.cpus();
}

// Get cpu architecture
function getCpuArch() {
  return os.arch();
}

// Get cpu model
function getCpuModel() {
  return getCpus()[0].model;
}

// Get cpu arch, model, core, usage percent
function getCpusSimple() {
  return new Promise(function(resolve, reject) {
    const model = getCpuModel();
    const arch = getCpuArch();
    let speed1, user1, nice1, sys1, idle1, irq1, total1;
    let speed2, user2, nice2, sys2, idle2, irq2, total2;
    let core = 0;
    speed1 = user1 = nice1 = sys1 = idle1 = irq1 = total1 = 0;
    speed2 = user2 = nice2 = sys2 = idle2 = irq2 = total2 = 0;

    getCpus().forEach(function(cpu) {
      core++;
      speed1 += cpu.speed;
      user1 += cpu.times.user;
      nice1 += cpu.times.nice;
      sys1 += cpu.times.sys;
      idle1 += cpu.times.idle;
      irq1 += cpu.times.irq;
    });
    total1 = speed1 + user1 + nice1 + sys1 + idle1 + irq1;

    // 1 second after
    setTimeout(function() {
      getCpus().forEach(function(cpu) {
        speed2 += cpu.speed;
        user2 += cpu.times.user;
        nice2 += cpu.times.nice;
        sys2 += cpu.times.sys;
        idle2 += cpu.times.idle;
        irq2 += cpu.times.irq;
      });

      total2 = speed2 + user2 + nice2 + sys2 + idle2 + irq2; 
      let idle = idle2 - idle1;
      let total = total2 - total1;
      let per = ((1 - idle/total) * 100).toFixed(2);

      resolve({
        'arch': arch,
        'model': model,
        'core': core,
        'percent': per
      });
    }, 1000);
  });
}

// Get free memory(Byte)
function getFreeMem() {
  return os.freemem();
}

// Get total memory(Byte)
function getTotalMem() {
  return os.totalmem();
}

// Get usage memory(Percent)
function getMemSimple() {
  return ((1 - getFreeMem() / getTotalMem()) * 100).toFixed(2);
}

// Get platform
function getPlatform() {
  return os.platform();
}

// var monitor = require('monitor.js').init();
exports.init = function() {
  return {
    'getCpus': getCpus,
    'getCpuArch': getCpuArch,
    'getCpuModel': getCpuModel,
    'getCpusSimple': getCpusSimple,
    'getFreeMem': getFreeMem,
    'getTotalMem': getTotalMem,
    'getMemSimple': getMemSimple,
    'getPlatform': getPlatform
  }
}